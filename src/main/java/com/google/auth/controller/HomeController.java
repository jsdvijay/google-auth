package com.google.auth.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {
    public  String home(){
        return "welcome to Google authentication Multifactor authentication ";
    }
}
