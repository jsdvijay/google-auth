package com.google.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.auth.service.MyCredentialRepositoryImpl;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

@RestController
@RequestMapping("/google-auth")
public class GoogleAuthController {
	
	
	@Autowired
	MyCredentialRepositoryImpl myCredentialRespository;
	
	@RequestMapping("/hello")
	public String sayHello() {
		return "Hello google authenticator !";
	}
	
	
   @GetMapping("/getsecretkey")
   public String getSecretKey(@RequestParam String userName) {
	   GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();
	   googleAuthenticator.setCredentialRepository(myCredentialRespository);
	   GoogleAuthenticatorKey googleAuthenticatorKey = googleAuthenticator.createCredentials(userName);
	   return "Secret Key "+googleAuthenticatorKey.getKey();
   }
   
   @GetMapping("/getotpcode")
   public String getOtpCode(@RequestParam String userName, @RequestParam String secret) {
	   GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();
	   int otpCode = googleAuthenticator.getTotpPassword(secret);
	   return "The opt code is "+ otpCode;
   }
	
   
   @GetMapping("/verifyotp")
   public Boolean verifyOpt(@RequestParam Integer otpCode, @RequestParam String secretKey) {
	   GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();
	   boolean isValidCode = googleAuthenticator.authorize(secretKey, otpCode);
	   return isValidCode;
	   
   }

}
