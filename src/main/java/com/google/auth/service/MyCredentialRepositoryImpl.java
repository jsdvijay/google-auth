package com.google.auth.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.ICredentialRepository;


@Component
public class MyCredentialRepositoryImpl implements ICredentialRepository {

	@Override
	public String getSecretKey(String userName) {
		// TODO Auto-generated method stub
		GoogleAuthenticator googleAuthenticator = new GoogleAuthenticator();
		
		googleAuthenticator.setCredentialRepository(this);
		
		return googleAuthenticator.createCredentials(userName).getKey();
	}

	@Override
	public void saveUserCredentials(String userName, String secretKey, int validationCode, List<Integer> scratchCodes) {
		// TODO Auto-generated method stub
	}

}
